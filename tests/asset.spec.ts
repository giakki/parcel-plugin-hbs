import path from 'path';

import HandlebarsAsset from '../src/asset';

const makeAsset = async (fixtureName: string) => {
    const asset = new HandlebarsAsset(path.join(__dirname, 'fixtures', fixtureName), { rootDir: __dirname });

    await asset.loadIfNeeded();

    return asset;
};

const makeMatch = (expectedValue: string) => [{ type: 'html', value: expectedValue }];

describe('asset', () => {
    afterEach(() => {
        process.env.NODE_ENV = 'development';
        delete process.env.__TEST_ENV_VAR__;
    });

    it('should construct', async () => {
        const asset = await makeAsset('html.hbs');

        return expect(asset.generate()).resolves.toBeTruthy();
    });

    it('should be able to interpolate env variables', async () => {
        const asset = await makeAsset('interpolate.hbs');
        const expectedValue = 'ok';

        process.env.__TEST_ENV_VAR__ = expectedValue;
        return expect(asset.generate()).resolves.toEqual(makeMatch(expectedValue));
    });

    it('should be able to use custom helpers (1)', async () => {
        const asset = await makeAsset('if.hbs');

        process.env.NODE_ENV = 'development';
        await expect(asset.generate()).resolves.toEqual(makeMatch('dev'));
    });

    it('should be able to use custom helpers (2)', async () => {
        const asset = await makeAsset('if.hbs');

        process.env.NODE_ENV = 'production';
        await expect(asset.generate()).resolves.toEqual(makeMatch('prod'));
    });

    it('should throw on missing env variable access', async () => {
        const asset = await makeAsset('missing.hbs');

        await expect(asset.generate()).rejects.toBeInstanceOf(Error);
    });
});
