declare module 'parcel-bundler' {
    export interface AssetOptions {
        rootDir: string;
    }

    export class Asset {
        contents: string;

        constructor(path: string, options: AssetOptions);

        loadIfNeeded(): Promise<void>;
    }

    export class ParcelBundler {
        addAssetType(extension: string, assetModule: string): void;
    }
}
