import handlebars from 'handlebars';
import { Asset, AssetOptions } from 'parcel-bundler';

function getContext(): Record<string, string | boolean> {
    const isProd = process.env.NODE_ENV === 'production';
    const isDev = !isProd;

    return {
        ...process.env,
        isDev: isDev,
        isProd: isProd,
    };
}

export = class HandlebarsAsset extends Asset {
    constructor(path: string, options: AssetOptions) {
        super(path, options);
    }

    async generate() {
        const compiled = handlebars.compile(this.contents, {
            strict: true,
        });

        return [
            {
                type: 'html',
                value: compiled(getContext()),
            },
        ];
    }
};
