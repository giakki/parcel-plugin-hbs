import { ParcelBundler } from 'parcel-bundler';

export = (bundler: ParcelBundler) => {
    bundler.addAssetType('hbs', require.resolve('./asset'));
};
